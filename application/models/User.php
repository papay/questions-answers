<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{

    public static function tableName()
    {
        return 'users';
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'name' => 'Имя',
        ];
    }

    public function rules()
    {
        return [
            [['username', 'password', 'name'], 'required', 'on' => 'register'],
            ['username', 'unique', 'on' => 'register'],

            ['username', 'required', 'on' => 'login', 'message' => 'Проверьте правильность логина'],
            ['password', 'required', 'on' => 'login', 'message' => 'Неверный пароль'],

            [['username', 'password', 'name'], 'trim'],
            [['username', 'password', 'name'], 'string'],
        ];
    }

    public function beforeSave($insert)
    {
        if (!isset($this->id)) {
            $this->password = \Yii::$app->security->generatePasswordHash($this->password);
        }

        return true;
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
}