<?php

use \yii\helpers\Url;
use \yii\bootstrap\Html;
use \yii\bootstrap\ActiveForm;

$answerModel = new \app\models\Answer();

?>
<div class="row">
    <hr/>
    <p><strong><?=$model->author->name ?></strong>, <em><?=Yii::$app->formatter->asDatetime($model->date, 'php:d.m.Y H:i:s') ?></em></p>
    <blockquote>
        <?=$model->text ?>
    </blockquote>
    <p>
        <?=Html::a('Посмотреть ответы <span class="badge">'.count($model->answers).'</span> / Ответить', '#answers_' . $model->id, [
            'data-toggle' => 'collapse',
            'aria-expanded' => 'false',
            'aria-controls' => 'answers_' . $model->id,
        ]) ?>
    </p>
    <div class="collapse" id="answers_<?=$model->id ?>">

        <?php if (Yii::$app->user->isGuest) : ?>
            <div class="well text-center">
                <?=Html::a('Авторизируйтесь', Url::toRoute('user/auth'), [
                    'data-toggle' => 'modal',
                    'data-target' => '#modal'
                ])?>, чтоб иметь возможность задавать вопросы и оставлять ответы.
            </div>
        <?php else : ?>

            <?php $form = ActiveForm::begin([
                'id' => 'question-form',
                'layout' => 'horizontal',
                'action' => '/index/answer/',
                'enableAjaxValidation' => false,
            ]); ?>

            <?php echo $form->field($answerModel, 'text')->textarea(); ?>

            <div class="pull-right">
                <?=Html::activeHiddenInput($answerModel, 'question_id', ['value' => $model->id]) ?>
                <?=Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
            <div class="clearfix"></div>

            <?php ActiveForm::end(); ?>

        <?php endif; ?>

        <?php foreach ($model->answers as $answer) {
            echo $this->render('_answer', ['model' => $answer]);
        } ?>
    </div>

</div>