<?php

use yii\db\Migration;

/**
 * Handles the creation for table `users`.
 */
class m161021_175159_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(30),
            'password' => $this->string(60),
            'name' => $this->string(30),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
