<?php

namespace app\controllers;

use app\models\Answer;
use app\models\Question;
use \yii\web\Controller;
use \yii\data\ActiveDataProvider;

class IndexController extends Controller
{
    public function actionIndex()
    {
        $this->view->title = 'Сервис вопросов и ответов';

        $data = new ActiveDataProvider([
            'query' => Question::find()->with('answers', 'author')->orderBy('date DESC'),
            'pagination' => [
                'forcePageParam' => false,
                'pageSizeParam' => false,
                'pageSize' => 10,
            ],
        ]);
        $question = new Question();

        echo $this->render('index', [
            'data' => $data,
            'questionModel' => $question,
        ]);
    }

    public function actionQuestion()
    {
        if (\Yii::$app->request->isPost) {
            $model = new Question();
            $postData = \Yii::$app->request->post('Question');
            $model->setAttributes($postData);
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', 'Ваш вопрос опубликован.');
            }
        }
        return $this->redirect('/');
    }

    public function actionAnswer()
    {
        if (\Yii::$app->request->isPost) {
            $model = new Answer();
            $postData = \Yii::$app->request->post('Answer');
            $model->setAttributes($postData);
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', 'Ваш ответ на вопрос успешно добавлен.');
            }
        }
        return $this->redirect('/');
    }
}