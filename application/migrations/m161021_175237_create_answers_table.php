<?php

use yii\db\Migration;

/**
 * Handles the creation for table `answers`.
 */
class m161021_175237_create_answers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('answers', [
            'id' => $this->primaryKey(),
            'text' => $this->text(),
            'author_id' => $this->integer(),
            'question_id' => $this->integer(),
            'date' => $this->timestamp(),
        ]);

        $this->createIndex('author_id', 'answers', 'author_id');
        $this->createIndex('question_id', 'answers', 'question_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('author_id', 'answers');
        $this->dropIndex('question_id', 'answers');
        $this->dropTable('answers');
    }
}
