<?php

namespace app\models;

use yii\db\ActiveRecord;

class Answer extends ActiveRecord
{

    public static function tableName()
    {
        return 'answers';
    }

    public function attributeLabels()
    {
        return [
            'text' => 'Ваш ответ',
        ];
    }

    public function rules()
    {
        return [
            ['text', 'required'],
            ['text', 'trim'],
            ['text', 'string'],
            ['question_id', 'required'],
            ['question_id', 'integer'],
        ];
    }

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

    public function beforeSave($insert)
    {
        $this->author_id = \Yii::$app->user->id;
        return true;
    }

}