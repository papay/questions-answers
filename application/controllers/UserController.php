<?php

namespace app\controllers;

use app\models\User;
use yii\bootstrap\ActiveForm;
use \yii\web\Controller;
use yii\web\Response;

class UserController extends Controller
{
    public function actionAuth()
    {
        $result = [];

        if (!\Yii::$app->user->isGuest) {
            return $this->redirect('/');
        }
        $model = new User();
        $model->setScenario('login');

        if (\Yii::$app->request->isPost) {

            $userData = \Yii::$app->request->post('User');
            $identity = User::findOne(['username' => $userData['username']]);

            if ($identity) {
                if (!empty($userData['password'])) {
                    if (\Yii::$app->security->validatePassword($userData['password'], $identity->password)) {
                        \Yii::$app->user->login($identity);
                        return $this->redirect('/');
                    } else {
                        $result['user-password'] = ['Неверный пароль'];
                    }
                } else {
                    $result['user-password'] = ['Неверный пароль'];
                }
            } else {
                $result['user-username'] = ['Проверьте правильность логина'];
            }

            if (\Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }
        }

        if (\Yii::$app->request->isAjax) {
            echo $this->renderAjax('auth', [
                'model' => $model
            ]);
        } else {
            echo $this->render('auth', [
                'model' => $model
            ]);
        }
    }

    public function actionRegister()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect('/');
        }
        $model = new User();
        $model->setScenario('register');

        if (\Yii::$app->request->isPost) {

            $userData = \Yii::$app->request->post('User');
            $model->setAttributes($userData);

            if (\Yii::$app->request->post('reg', false)) {
                if ($model->save()) {
                    \Yii::$app->user->login($model);
                } else {
                    var_dump($model->getErrors());
                }
            }
        }
        return $this->redirect('/');
    }

    public function actionRegisterValidate()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (\Yii::$app->request->isAjax) {

            if (\Yii::$app->user->isGuest) {
                $model = new User();
                $userData = \Yii::$app->request->post('User');
                $model->setAttributes($userData);

                return ActiveForm::validate($model);
            }
        }
        return [];
    }

    public function actionLogout()
    {
        if (!\Yii::$app->user->isGuest) {
            \Yii::$app->user->logout();
        }
        return $this->redirect('/');
    }
}