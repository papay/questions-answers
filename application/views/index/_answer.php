<div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 pull-right">
    <p><strong><?=$model->author->name ?></strong>, <em><?=Yii::$app->formatter->asDatetime($model->date, 'php:d.m.Y H:i:s') ?></em></p>
    <blockquote>
        <?=$model->text ?>
    </blockquote>
</div>