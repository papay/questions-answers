### Install Composer dependencies###
```
composer install
```

### Create DB. ###
Create database named 'qa'. And run `yii migrate` from project root directory.


### Redirecting to slash URLs. ###

via Nginx
```
location / {
    rewrite ^(.*[^/])$ $1/ permanent;
    try_files $uri $uri/ /index.php?$args;
}
```

via Apache
```
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.*[^/])$ /$1/ [L,R=301]
```