<?php

namespace app\controllers;

use \yii\web\Controller;

class ErrorController extends Controller
{
    public function actionIndex()
    {

    }

    public function actionError()
    {
        $exception = \Yii::$app->errorHandler->exception;

        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception, 'handler' => new \yii\web\ErrorHandler()]);
        }
    }
}