<?php

namespace app\models;

use yii\db\ActiveRecord;

class Question extends ActiveRecord
{

    public static function tableName()
    {
        return 'questions';
    }

    public function attributeLabels()
    {
        return [
            'text' => 'Ваш вопрос',
        ];
    }

    public function rules()
    {
        return [
            ['text', 'required'],
            ['text', 'trim'],
            ['text', 'string', 'min' => 10],
        ];
    }

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['question_id' => 'id'])->orderBy('date DESC');

    }

    public function beforeSave($insert)
    {
        $this->author_id = \Yii::$app->user->id;
        return true;
    }

}