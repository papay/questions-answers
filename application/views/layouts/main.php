<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

$this->beginPage();

?>
<!doctype html>
<html lang="ru">
    <head>
        <meta charset="<?= Yii::$app->charset; ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags(); ?>
        <title><?= Html::encode($this->title); ?></title>
        <?php $this->head(); ?>
    </head>
    <body>
        <?php $this->beginBody(); ?>
        <?php NavBar::begin([
            'brandLabel' => 'Questions & Answers'
        ]); ?>
            <?php if (!Yii::$app->user->isGuest) : ?>
                <p class="navbar-text pull-right">Вы вошли, как <?=Yii::$app->user->identity->name ?>. <?php echo Html::a('Выйти', '/user/logout/'); ?></p>
            <?php endif; ?>
            <?php echo Nav::widget([
                'id' => 'mainmenu',
                'options' => [
                    'class' => 'nav-pills pull-right'
                ],
                'items' => [
                    [
                        'label' => 'Войти',
                        'url' => ['user/auth'],
                        'visible' => Yii::$app->user->isGuest,
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                            'data-target' => '#modal'
                        ],
                    ],
                ],
            ]);?>
        <?php NavBar::end();?>
        <?= $content; ?>
        <footer></footer>
        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>