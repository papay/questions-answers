<?php

use \yii\helpers\Url;
use \yii\bootstrap\Html;
use \yii\widgets\Pjax;
use \yii\widgets\ListView;
use \yii\bootstrap\ActiveForm;

?>
<div class="container">

    <?php

    $flashMessages = \Yii::$app->session->getAllFlashes();
    foreach ($flashMessages as $type => $message) {
        echo "<div class='alert alert-{$type}' role='alert'>{$message}</div>";
    }

    ?>

    <div class="jumbotron">
        <h2>Добро пожаловать на сайт вопросов и ответов</h2>
        <p>
            Здесь Вы найдете ответы на все интересующие Вас вопросы.<br/>
            А если вдруг не найдете, то можете задать вопрос, и знающие люди ответят Вам.
        </p>
    </div>

    <?php Pjax::begin(['id' => 'questionsList']) ?>
        <?php echo ListView::widget([
            'dataProvider' => $data,
            'itemView' => '_question',
            'summary' => 'Всего вопросов в нашей базе: {totalCount}',
            'layout' => "<h3>{summary}</h3><br>\n{items}\n<div class='text-center'>{pager}</div>"
        ]); ?>
    <?php Pjax::end() ?>

    <?php if (Yii::$app->user->isGuest) : ?>
        <div class="well text-center">
            <?=Html::a('Авторизируйтесь', Url::toRoute('user/auth'), [
                'data-toggle' => 'modal',
                'data-target' => '#modal'
            ])?>, чтоб иметь возможность задавать вопросы и оставлять ответы.
        </div>
    <?php else : ?>
        <div class="well text-center">
            <p>Задайте вопрос</p>
            <?php $form = ActiveForm::begin([
                'id' => 'question-form',
                'layout' => 'horizontal',
                'action' => '/index/question/',
                'enableAjaxValidation' => false,
            ]); ?>

            <?php echo $form->field($questionModel, 'text')->textarea(); ?>

            <hr/>
            <div class="pull-right">
                <?=Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
            <div class="clearfix"></div>

            <?php ActiveForm::end(); ?>
        </div>
    <?php endif; ?>
</div>


<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>