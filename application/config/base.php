<?php

return [
    'id' => 'q_a',
    'basePath' => __DIR__ . '/..',
    'language' => 'ru',
    'vendorPath' => '@app/../vendor',
    'components' => [
        'request' => [
            'class' => 'yii\web\Request',
            'cookieValidationKey' => 'sYy9nbcs8SgqYGdV6ZnWjBAh',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\models\User',
        ],
        'errorHandler' => [
            'errorAction' => 'error/error',
        ],
        'urlManager' => require(__DIR__ . '/routes.php'),
        'charset' => 'utf-8',
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'memcache' => [
            'class' => 'yii\caching\MemCache',
        ],
        'db' => require(__DIR__ . '/db_' . YII_ENV . '.php'),
    ],
    'on beforeRequest' => function () {
        $pathInfo = Yii::$app->request->pathInfo;
        $query = Yii::$app->request->queryString;
        if (!empty($pathInfo) && substr($pathInfo, -1) !== '/') {
            $url = '/' . $pathInfo . '/';
            if ($query) {
                $url .= '?' . $query;
            }
            Yii::$app->response->redirect($url, 301);
            Yii::$app->end();
        }
    },
];