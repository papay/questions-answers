<?php

return [
    'id' => 'partner-console',
    'bootstrap' => ['log', 'gii'],
    'basePath' => __DIR__ . '/..',
    'language' => 'ru',
    'timeZone' => 'Europe/Kiev',
    'controllerNamespace' => 'yii\console\controllers',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'charset' => 'utf-8',
        'log' => [
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'memcache' => [
            'class' => 'yii\caching\MemCache',
        ],
        'db' => require(__DIR__ . '/db_' . YII_ENV . '.php'),
    ],
];