<?php

use yii\db\Migration;

/**
 * Handles the creation for table `questions`.
 */
class m161021_175225_create_questions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('questions', [
            'id' => $this->primaryKey(),
            'text' => $this->text(),
            'author_id' => $this->integer(),
            'date' => $this->timestamp(),
        ]);

        $this->createIndex('author_id', 'questions', 'author_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('author_id', 'questions');
        $this->dropTable('questions');
    }
}
